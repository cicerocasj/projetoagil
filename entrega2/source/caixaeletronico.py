# coding:utf8


class CaixaEletronico(object):
    def __init__(self, servico, hardware):
        self.servico = servico
        self.hardware = hardware
        self.conta = object()

    def saldo(self):
        return "O saldo é R$%.2f" % self.conta.saldo

    def sacar(self, valor):
        if valor <= self.conta.saldo:
            self.conta.saldo -= valor
            self.servico.persistirConta(self.conta)
            return "Retire seu dinheiro"
        return "Saldo insuficiente"

    def depositar(self, valor):
        self.conta.saldo += valor
        return "Depósito recebido com sucesso"

    def login(self, numero):
        conta = self.servico.recuperarConta(numero)
        if conta and conta.numero == numero:
            self.conta = conta
            return "Usuário Autenticado"
        return "Não foi possível autenticar o usuário"
