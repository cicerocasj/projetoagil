# coding:utf8
import os
import sys
import unittest
import mockhardware
import mockservicoremoto
# adiciona o path da raiz do projeto
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from source import caixaeletronico, contacorrente


class Testes(unittest.TestCase):
    def setUp(self):
        mock_servico = mockhardware.MockServicoRemoto()
        mock_servico.contas.append(contacorrente.ContaCorrente('1', 10))
        mock_servico.contas.append(contacorrente.ContaCorrente('2', 20))

        mock_hardware = mockservicoremoto.MockHardware(contacorrente.ContaCorrente('1', 10))
        self.caixa_eletronico = caixaeletronico.CaixaEletronico(mock_servico, mock_hardware)

    def login(self):
        self.caixa_eletronico.login("1")

    def teste_saldo(self):
        self.login()
        self.assertEqual(self.caixa_eletronico.saldo(), "O saldo é R$10.00")

    def teste_saque(self):
        self.login()
        self.caixa_eletronico.sacar(5)
        self.assertEqual(self.caixa_eletronico.saldo(), "O saldo é R$5.00")

    def teste_deposito(self):
        self.login()
        self.caixa_eletronico.depositar(15)
        self.assertEqual(self.caixa_eletronico.saldo(), "O saldo é R$25.00")

    def teste_conta_invalida(self):
        resultado = self.caixa_eletronico.login("cicero")
        self.assertEqual(resultado, "Não foi possível autenticar o usuário")

    def teste_hardware(self):
        self.login()
        self.caixa_eletronico.hardware.mau_funcionamento = True
        try:
            self.caixa_eletronico.hardware.lerEnvelope()
        except Exception as e:
            self.assertRaises(Exception, e)

if __name__ == '__main__':
    unittest.main(verbosity=3)
