# coding:utf8


class MockServicoRemoto(object):
    def __init__(self):
        self.contas = list()

    def persistirConta(self, conta):
        nova_conta = self.recuperarConta(conta.numero)
        for i, conta in enumerate(self.contas):
            if conta.numero == nova_conta.numero:
                self.contas[i] = nova_conta
                break

    def recuperarConta(self, numero):
        for conta in self.contas:
            if conta.numero == numero:
                return conta


class MockHardware(object):
    def __init__(self, conta):
        self.conta = conta
        self.mau_funcionamento = bool(False)

    def pegarNumeroDaContaCartao(self, numero):
        if self.mau_funcionamento:
            raise Exception("Mau funcionamento de hardware")
        return self.conta.numero

    def entregarDinheiro(self, numero):
        if self.mau_funcionamento:
            raise Exception("Mau funcionamento de hardware")

    def lerEnvelope(self):
        if self.mau_funcionamento:
            raise Exception("Mau funcionamento de hardware")
